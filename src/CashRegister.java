/**
 * Created by Dmitry on 7/13/2015.
 */
public class CashRegister {
    RetailItem item;
    int unitsSold;

    final double TAX = 0.06;

    public CashRegister(RetailItem item, int unitsSold) {
        this.item = item;
        this.unitsSold = unitsSold;
    }

    public double getSubtotal() {
        return  item.getPrice() * unitsSold;
    }

    public double getTax() {
        return getSubtotal() * TAX;
    }

    public double getTotal() {
        return getSubtotal() + getTax();
    }
}
