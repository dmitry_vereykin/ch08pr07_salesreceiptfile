/**
 * Created by Dmitry on 7/15/2015.
 */
import java.text.DecimalFormat;
import java.io.*;

public class PrintReceipt {
    private String description;
    private int unitsSold;
    private double price;
    private double subtotal;
    private double tax;
    private double total;

    public PrintReceipt(String description, int unitsSold, double price, double subtotal, double tax, double total) {
        this.description = description;
        this.unitsSold = unitsSold;
        this.price = price;
        this.subtotal = subtotal;
        this.tax = tax;
        this.total = total;
    }

    public void printInFile() throws IOException {
        File outputFile = new File("Sales Receipt.txt");
        DecimalFormat dollar = new DecimalFormat("#,###.00");

        if(!outputFile.exists()) {
            outputFile.createNewFile();
        }

        PrintWriter salesReceipt = new PrintWriter(outputFile);

        salesReceipt.println("SALES RECEIPT");
        salesReceipt.println("-----------------");
        salesReceipt.println("Item: " + description);
        salesReceipt.println("Unit Price: $" + dollar.format(price));
        salesReceipt.println("Quantity: " + unitsSold);
        salesReceipt.println("Subtotal: $" + dollar.format(subtotal));
        salesReceipt.println("Sales Tax: $" + dollar.format(tax));
        salesReceipt.println("Total: $" + dollar.format(total));

        salesReceipt.close();
    }
}
